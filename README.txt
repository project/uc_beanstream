1. SUMMARY

  The Beanstream module provides integration between Ubercart 2.x
  and the Beanstream Gateway, provided by beanstream.com and
  TD Canada Trust Online Mart.


2. REQUIREMENTS
	
  * A Beanstream account for credit card processing services, and a compatible
    merchant account. - http://beanstream.com/
  * Ubercart 2.x - http://drupal.org/project/ubercart
  * The PHP cURL library must be installed. 


3. INSTALLATION

   Install as usual, see http://drupal.org/node/70151 for further information.


4. CONFIGURATION

  Edit the settings in Store Administration > Configuration > Payment Settings >
  Payment Gateways.  Expand the Beanstream section and fill in the required
  information:

  * Merchant ID
    This is the merchant id that you are using with Beanstream.

  * Transaction mode
    Set to Test or Production based on your Beanstream setup.  When you first
    open a Beanstream account, it will be set up for test payments, and you will
    receive test credit card numbers that you can use to ensure the gateway is
    working.  Then you will need to contact Beanstream to have your service
    production-enabled.  Switch this setting at that time.

  * Customer Notifications
    Whether the Beanstream merchant service should send the customer an email
    upon checkout. This is in addition to the Ubercart confirmation email.

  * Merchant Notifications
    Whether the Beanstream merchant service should send you an email upon
    checkout. This is in addition to the Ubercart confirmation email.


5. CONTACT

  Maintainers:
  * Arvana Robinson (arvana) - http://drupal.org/user/39133
